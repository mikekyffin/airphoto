README
------

Repo for work on the Air Photo portion of the Regional Environmental History Atlas (REHA), a project of the Maps, 
Data and Government Information Centre (MaDGIC) at Trent University's Bata Library.

Development site: http://madgic3.trentu.ca/airphoto/
Production site: http://madgic.trentu.ca/airphoto/

Links:
	http://www.trentu.ca/library/madgic/reha.htm
	https://www.trentu.ca/
	https://www.trentu.ca/library/
	https://www.trentu.ca/library/madgic/

