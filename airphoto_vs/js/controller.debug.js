﻿// file:            controller.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    //
    app.controller = {
        // return a model from the
        find: function (_name) {
            return $.map(this.Controllers, function (controller) {
                return (controller.name == _name) ? controller : null;
            })[0];
        },
        //
        add: function (controller) {
            this.Controllers.push(controller);
            app.map.Map.addLayer(controller.footprint);
            app.map.Map.addLayer(controller.layer);
        },
        // array of models on the map
        Controllers: [],
        // the controller functionality between model from the server and the view (DOM + Layer) on the client
        // TODO - add Map as a arg and reference internally
        Controller: function (params) {
            var _layer = new app.layers.Layer(params);
            //var _footprintUrl = params.url.substring(0, params.url.lastIndexOf("/")) + "_footprint" + "/" + "MapServer";
            var _footprintUrl = params.url + "/1";

            // OPTION #1 for FOOTPRINT
            // call the sublayer that references the footprint data and build use as a graphics layers.
            // note MODE_SNAPSHOT - call the whole shape at once and provide for the browser - and *never*
            // call AGS again to upate the shape. the geometries are relateively simple, so just load them.
            //  if we were didplaying a hi-res outline of Nunavut, we probably wouldn't do this.
            var _footprint = new esri.layers.FeatureLayer(_footprintUrl, {
                id: params.name + "_footprint",
                mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
                outFields: [],
                opacity: 0.55,
                visible: false
            });
           
            // OPTION #2 for FOOTPRINT
            // simple instantiate another layer from the service, but only show sublayer([1]) and ignore the phothos 
            // completely. this works - but everytime the user pans/zooms to a new portion of the image a server
            // process is spawned to go make the PNG/JPG of the area ad return to the client map. normally this is 
            // totally fine, but remember that we're trying to reduce load on the server - so every call to the service 
            // needs an instance of the service to read from, and that's what we're trying to reduce.
            //var _footprint = new esri.layers.ArcGISDynamicMapServiceLayer(_footprintUrl, {
            //    id: params.name + "_footprint",
            //    opacity: 0.55,
            //    visible: false
            //});
            //_footprint.setVisibleLayers([1]);
           
            return {
                name: params.name,
                footprint: _footprint,
                layer: _layer,
                model: params,
                view: new app.views.View(params, _layer, _footprint),
                initialize: function (_node) {
                    // place in DOM @ _node
                    this.render(_node);
                },
                // return the URL from the model
                getUrl: function () {
                    return params.url;
                },
                // place in the DOM
                render: function (node) {
                    this.view.place(node);
                },
                remove: function () {
                    // TODO
                },
                add: function () {
                    app.map.Map.addLayer(this.layer);
                },
                downloadImage: function (imageName, year) {
                    var params = new app.download.ImageDownloadParameters(imageName, year);
                    var download = new app.download.ImageDownload(params);
                    download.submitJob();
                },
                onIdentifyStart: function () {
                    this.view.working(true);
                },
                onIdentifyEnd: function () {
                    this.view.working(false);
                },
                identify: function (evt) {
                    this.onIdentifyStart();
                    var note; 
                    note = $.notify.note(this.layer.year + ": searching...", "info", 5);
                    var ident = new madgic.esri.Identify(this.layer.url, app.config.PROXY, true);
                    var xhr = ident.doIdentify(
                        0,
                        evt.mapPoint.x,
                        evt.mapPoint.y,
                        app.map.Map.spatialReference.wikd,
                        app.map.Map.extent
                    );
                    var ll = esri.geometry.xyToLngLat(evt.mapPoint.x, evt.mapPoint.y)
                    app.ga.track({
                        'eventCategory': "Identify", // required.                
                        'eventAction': ll[1] + "," + ll[0], // required.      
                        'eventLabel': this.layer.year,
                        'eventValue': null
                    });
                    xhr.done($.proxy(function (data) {
                        note.remove(); 
                        this.view.emptyTiles(); 
                        note = $.notify.note(this.layer.year + ": " + "found " + parseInt(data.results.length) + " air photos", (data.results.length === 0) ? "warning" : "info", 5);
                        if (data.results.length > 0) {
                            $.each(data.results, $.proxy(function (i, feature) {
                                var te = new this.view.TileElement(feature);
                                te.place();
                            }, this));
                        } else {
                            //this.layer.setTiles([]);
                        }
                    }, this));
                    xhr.fail($.proxy(function (err) {
                        note = $.notify.note(this.layer.year + ": error searching this year", "error", 15);
                    }, this));
                    xhr.always($.proxy(function () {
                        this.onIdentifyEnd();
                    }, this));
                }
            }
        }
    }
})(jQuery);