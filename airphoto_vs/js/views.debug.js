﻿// file:            views.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    //
    app.views = {
        //
        setTilesUpdateStart: function () {
            $(".tile-element .tile-roundel input:checked")
                .parent()
                .siblings(".tile-loader")
                .children(".tile-loader-image")
                .css("display", "inline-block");
        },
        //
        setTilesUpdateEnd: function() {
            $(".tile-element .tile-roundel input:checked")
                .parent()
                .siblings(".tile-loader")
                .children(".tile-loader-image")
                .css("display", "none");
        },
        // the big checkbox thing
        Roundel: function (obj) {
            var _round = $("<div>").addClass("roundCheck");
            var _input = $("<input>", {
                type: "checkbox",
                id: obj.layerName + "_" + obj.value.substring(0, obj.value.lastIndexOf(".")),
                value: obj.value,
                name: "check"
            }).appendTo(_round);
            var _label = $("<label>", {
                "for": obj.layerName + "_" + obj.value.substring(0, obj.value.lastIndexOf("."))
            }).appendTo(_round);
            return _round;

        },
        // the ON/OFF slider checkbox thing.
        // used to turn the footprint on and off - status
        // allows the associated tile layer to be identified/queried
        // or not.
        SliderCheckbox: function (obj, label, textOffset) {
            var _slider = $("<div>").addClass("onOff");
            var _input = $("<input>", {
                type: "checkbox",
                id: obj.title + "_" + label,
                value: obj.id,
                name: obj.title + "_layer"
            }).appendTo(_slider);
            var _label = $("<label>", {
                "for": obj.title + "_" + label,
                "html": $("<span class=\"slider-label-text\" style=\"position:relative;left:" + textOffset + "\">" + label + "</span>")
            }).appendTo(_slider);
            return _slider;
        },
        // the main view of a "conceptual layer" - which in this case is
        // two map layers - of a footprint and dynamic list of tiles from
        // another map service.
        View: function (data, layer, footprint) {
            // holds everything - the view "container"
            var _view = $("<div>", {
                layer: layer.id
            }).addClass("view");
            // a handy wrap - could be used later for loading opacity masks, colour changes, etc.    
            var _wrap = $("<div>")
                .addClass("view-wrap")
                .appendTo(_view);
            // the top row of the view
            var _top = $("<div>")
                .addClass("view-top")
                .appendTo(_wrap);
            // to hold the layer working spinner
            var _load = $("<div>", {
                html: ""
            })
                .addClass("view-load")
                .appendTo(_top);
            // the image
            var _img = $("<img>", {
                src: "images/gear_FEFEFE_5F89AA_20x20.gif"
            })
                .addClass("view-img")
                .appendTo(_load);
            // the title
            var _title = $("<div>", {
                html: data.year
            })
                .addClass("view-title")
                .appendTo(_top);
            // the minimize icon - show title bar only
            var _minBtn = $("<span>", {
                title: "Minimize"
            })
                .click(function () {
                    _bottom.fadeOut(200);
                    _minBtn.css("display", "none");
                    _maxBtn.css("display", "block");
                    app.ga.track({
                        "eventCategory": "Layer",
                        "eventAction": "minimize",
                        "eventLabel": data.year,
                        "eventValue": null
                    });
                })
                .addClass("ui-icon ui-icon-circle-minus view-btn");
            // the maximize button - show the layer view contents when hidden
            var _maxBtn = $("<span>", {
                title: "Maximize",
                style: "display: none;"
            })
                .click(function () {
                    _bottom.fadeIn(200);
                    _minBtn.css("display", "block");
                    _maxBtn.css("display", "none");
                    app.ga.track({
                        "eventCategory": "Layer",
                        "eventAction": "maximize",
                        "eventLabel": data.year,
                        "eventValue": null
                    });
                })
                .addClass("ui-icon ui-icon-circle-plus view-btn");
            // open the metadata in the dialog
            var _infoBtn = $("<span>", {
                title: "Metadata"
            })
                .click(function () {
                    app.ui.setDialogTitle(data.year);
                    app.ui.setDialogContent(data);
                    // build the content for the dialog from the metadata already downloaded via the menu 
                    // builder stuff. at some point this will have to be totally replaced  by a call to
                    // a real metadata service.
                    var content = $.map(["summary", "description", "credits"], function (val, i) {
                        return $("<p>", {
                            html: "<b>" + (val.charAt(0).toUpperCase() + val.slice(1)) + "</b>: " + data[val]
                        });
                    });
                    app.ui.setDialogContent(content);
                    app.ui.openDialog();
                    // track
                    app.ga.track({
                        "eventCategory": "Layer",
                        "eventAction": "metadata",
                        "eventLabel": data.year,
                        "eventValue": null
                    });
                })
                .addClass("ui-icon ui-icon-info view-btn");
            // zoom to the full extent of the layer
            var _zoomBtn = $("<span>", {
                title: "Zoom to"
            })
                .click(function () {
                    app.map.Map.setExtent((app.map.Map.getLayer(data["name"] + "_footprint").fullExtent).expand(1.5));
                    // track
                    app.ga.track({
                        "eventCategory": "Layer",
                        "eventAction": "zoom",
                        "eventLabel": data.year,
                        "eventValue": null
                    });
                })            
                .addClass("ui-icon ui-icon-circle-zoomout view-btn");
            // make a div to hold all the buttons
            var _btns = $("<div>")
                .addClass("view-btns")                
                .appendTo(_top);
           // ... and add them
            _maxBtn.appendTo(_btns);
            _minBtn.appendTo(_btns);
            _infoBtn.appendTo(_btns);
            _zoomBtn.appendTo(_btns);
            // the larger portion of the view
            var _bottom = $("<div>")
                 .addClass("view-bottom")
                 .appendTo(_wrap);
            // holds the actual content
            var _content = $("<div>")
                .addClass("view-content")
                .appendTo(_bottom);
            // to hold footprint stuff
            var _footprint = $("<div>")
                .addClass("view-footprint")
                .appendTo(_content);
            // to hold layer stuff stuff
            var _layer = $("<div>")
                .addClass("view-footprint")
                .appendTo(_content);
            // this is the super-modified checkbox to show/hide the footprint
            var _check = new app.views.SliderCheckbox(data, "Footprint", "4px");
            _check.addClass("view-footprint-slider");
            _check.change(function () {
                // find the right controller
                var controller = app.controller.find(data["name"]);
                if ($("input", this).prop("checked")) {
                    controller.footprint.show();
                    // track adding the footprint
                    app.ga.track({
                        "eventCategory": "Footprint",
                        "eventAction": "show",
                        "eventLabel": data.year,
                        "eventValue": null
                    });
                } else {
                    controller.footprint.hide();
                }
            })
            _check.appendTo(_footprint);
            // this is the super-modified checkbox to show/hide the layer
            var _checkT = new app.views.SliderCheckbox(data, "Photos", "12px");
            _checkT.addClass("view-photos-slider");
            _checkT.change(function () {
                // find the right controller
                var controller = app.controller.find(data["name"]);
                if ($("input", this).prop("checked")) {
                    controller.layer.show();
                    if (app.map.LastClick == null) {
                        layer.setTiles([]);
                    } else {
                        layer.setTiles([]);
                        controller.identify(app.map.LastClick);
                    }
                    // track adding the layer
                    app.ga.track({
                        "eventCategory": "Layer",
                        "eventAction": "show",
                        "eventLabel": data.year,
                        "eventValue": null
                    });
                } else {
                    // set the layer definitions to be empty
                    controller.layer.setTiles([]);
                    // hide it from view - cannot show air photos now 
                    controller.layer.hide();
                    // remove the list from the UI
                    controller.view.emptyTiles();
                }
            })
            _checkT.appendTo(_layer);
            // // this is the super-modified slider to send opacity updates to the layer
            var _opacity = $("<div>")
                .slider({
                    min: 0,
                    max: 100,
                    value: 100,
                    slide: function () {
                        layer.setOpacity($(this).slider("value") / 100);
                    },
                    stop: function (evt) {
                        $.notify.note(layer.year + ": is now " + (100 - parseInt($(this).slider("value"))) + "% transparent", "info", 3);
                    }
                })
                .addClass("view-opacity-slider")
                .appendTo(_layer);
            // a list to hold all the found air photo at a given location
            var _tiles = $("<ul>")
                .addClass("view-tile-list")
                .appendTo(_content);
            // tidyness
            var _clear1 = $("<div>", {
                style: "clear: both;"
            }).appendTo(_wrap);
            //
            return {
                // show if the layer is updating, searching, etc.
                working: function (bool) {
                    (bool) ? _img.css("display", "inline-block") : _img.css("display", "none");
                },
                // is the layer in 'show' mode?
                isChecked: function () {
                    return $("input", _checkT).prop("checked");
                },
                // set the later as 'show'
                setChecked: function (bool) {                  
                    (bool) ? $("input", _checkT).prop("checked", true) : $("input", _checkT).prop("checked", false);
                    _checkT.change();
                },
                // place in the DOM
                place: function (node) {
                    node.prepend(_view);
                },
                // empty the list of TileElement nodes in the DOM
                emptyTiles: function () {
                    _tiles.empty();
                },
                //
                tiles: _tiles,
                //
                TileElement: function (feature) {
                    // reference the correct controller
                    var _controller = app.controller.find(feature["layerName"]);
                    // the parent <li>
                    var _elem = $("<li>")
                        .addClass("tile-element");
                    // the "working" image
                    var _load = $("<div>")
                        .addClass("tile-loader")
                        .appendTo(_elem);

                    var _img = $("<img>", {
                        src: "images/gear_FEFEFE_5F89AA_28x28.gif"
                    })
                        .addClass("tile-loader-image")
                        .appendTo(_load);
                    // the roundel-style checkbox
                    var _r = new app.views.Roundel(feature);
                    _r.addClass("tile-roundel");
                    // define the function that fires on change of the checkbox state
                    _r.change(function () {
                        // friendly note
                        $.notify.note(layer.year + ": rendering photo(s)", "info", 5);
                        // turn on the loading spinner for this photo
                        _img.css("display", "inline-block");
                        // map all the checked ones and send them to the layer for "becoming visible"
                        var arr = $.map($("input:checked", _tiles), function (node, i) {
                            return node.value
                        });
                        // not sure we even want something like this
                        //var startHook = dojo.connect(_controller.layer, "onUpdateStart", this, function () {
                        //    $.notify.note("{0}: adding photo {1}".format(layer.year, feature.attributes["Name"]), "info", 3);
                        //    dojo.disconnect(startHook);
                        //});
                        // setup a hook to define what's going to happen after the tile is displayed
                        var endHook = dojo.connect(_controller.layer, "onUpdateEnd", this, function () {
                            _img.css("display", "none");
                            dojo.disconnect(endHook);
                        });                       
                        // set all the tiles as appropriate
                        _controller.layer.setTiles(arr);
                    });
                    _r.appendTo(_elem);

                    // this is the download icon
                    var _dload = $("<div>", {
                        html: "<div class=\"inner\"><div style=\"position:relative;bottom:3px;\"><i class=\"fa fa-download\"></i></div></div>"
                    })
                        .addClass("tile-download")
                        .click(function () {
                            _controller.downloadImage(feature.value, feature.layerName.substring(1));
                            if (!app.download.hasVisited) {
                                app.ui.Tabs.setSelected("downloadTab");
                                app.download.hasVisited = true;
                            }
                        })
                        .appendTo(_elem);

                    var _meta = $("<div>", {
                        html: "<div class=\"inner\"><div style=\"position:relative;bottom:3px;\"><i class=\"fa fa-info\"></i></div></div>"
                    })
                        .addClass("tile-meta")
                        .click(function () {
                            // track the metadata request
                            app.ga.track({
                                "eventCategory": "Metadata",
                                "eventAction": feature.value,
                                "eventLabel": parseInt(feature.layerName.replace("y", "")),
                                "eventValue": null
                            });
                            // get the metadata from OpenShift
                            app.ui.setDialogFromAsyncUrl(app.config.PROXY + "?" + app.config.METADATA_OS + app.ui.nameToId(feature["value"]), feature["value"]);
                            // get the metadata from our NAPL index service - much slower
                            // uncomment this stuff to use NAPL service on our servers
                            //app.ui.setDialogFromAsyncUrl(app.config.METADATA + "?" + $.param({
                            //    "f": "json",
                            //    "outFields": "*",
                            //    "where": app.ui.nameToQuery(feature["value"])
                            //}), feature["value"]);
                            app.ui.openDialog();
                        })
                        .appendTo(_elem);

                    // the name of the tile itself
                    var _fileName = $("<div>", {
                        html: function () {
                            var title = $("<span>", {
                                html: feature["value"]
                            });
                            return title;
                        }
                    })
                        .addClass("tile-title")
                        .appendTo(_elem);

                    return {
                        working: function () {
                            (bool) ? _img.css("display", "inline-block") : _img.css("display", "none");
                        },
                        domNode: _elem,
                        place: function () {
                            // begin line mask here
                            // this stuff is to fade in the tiles found in sequence - to give the user an
                            // impression of action. The results retured can be written into the DOM much faster (and are),
                            // but this is another way of letting them know that something has happened.
                            var fadeModifier = $("li", _controller.view.tiles).length;
                            _elem.appendTo(_tiles);
                            _elem.fadeIn(400 + (fadeModifier * 200), function () {
                                // animation complete callback here
                                // remove line mask here
                            });
                        }
                        //},
                        //setChecked: function (bool) {
                        //    (bool) ? _elemCheck.prop("checked", true) : _elemCheck.prop("checked", false);   
                        //}
                    };
                }
            }
        }       
    }
    /*
        copied from reha - and also the styles in main.less
        */
    app.views.DownloadView = function (/*Object*/ obj) {
        this.model = obj;
        // container div for the view
        this.view = $("<div>")
            .addClass("download-view");
        // top row

        var _row1 = $("<div>")
            .appendTo(this.view);
        // top row contents
        var _ul = $("<div>", {
            "class": "download-view-icon"
        }).appendTo(_row1);

        this.spinner = $("<img>", {
            "class": "download-view-spinner",
            "src": "images/gear_5F89AA_FEFEFE_16x16.gif"
        }).appendTo(_ul);

        var _ur = $("<div>", {
            html: obj.title,
            "class": "download-view-text"
        }).appendTo(_row1);

        // bottom row
        var _row2 = $("<div>")
            .appendTo(this.view);

        var _ll = $("<div>", {
            "class": "download-view-icon"
        }).appendTo(_row2);

        this.info = $("<span>")
            .appendTo(_ll);

        var _lr = $("<div>", {
            "class": "download-view-text"
        }).appendTo(_row2);

        this.message = $("<span>", {
            "html": "Initializing download..."
        }).appendTo(_lr);
    }
    // can be "info", "error" or "complete"
    app.views.DownloadView.prototype.statusIcon = function (/*String*/ str) {
        switch (str) {
            case "info":
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-info");
                break;
            case "error":
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-alert");
                break;
            case "complete":
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-circle-check");
                break;
            default:
                this.info.removeClass();
                this.info.addClass("ui-icon ui-icon-info");
        }
    }
    // update the message in the view
    app.views.DownloadView.prototype.addMessage = function (/*String or DomNode*/ message) {
        this.message.html(message);
    }
    // place in DOM
    app.views.DownloadView.prototype.place = function (node) {
        node.prepend(this.view);
        this.view.fadeIn();
    }
    // remove
    app.views.DownloadView.prototype.destroy = function () {
        this.view.remove();
    }
    // ui bahaviours for download error
    app.views.DownloadView.prototype.onError = function (msg) {
        this.spinner.css("display", "none");
        this.statusIcon("error");
        this.addMessage(msg);
    }
    // completion of download operation - provide proxied link
    // TODO - get year of topo or provide 0
    app.views.DownloadView.prototype.onComplete = function (data) {
        this.spinner.css("display", "none");
        this.statusIcon("complete");
        var link = $("<a>", {
            //href: data.value.url,
            href: app.config.DOWNLOAD_AUTH + app.config.DOWNLOAD_DISCLAIMER + "?" + $.param({
                "proj": "airphoto",
                "year": 0, //test! 
                "file": data.value.url
            }),
            html: "Click to download",
            target: "_blank"
        });
        this.addMessage(link);
        $.notify.note(this.model["title"] + ".zip is now available!", "success", 10);
    }
})(window.jQuery);