﻿// file:            browser.debug.js
// author(s):       mike kyffin
// build target:    browser.js
//
// Copyright (c) 2013-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var browser = (function (d) {
    var _unsupportedMessage = "This browser is not supported and the application may not behave as designed.\n\nPlease consider using Firefox 10+, IE10+, Google Chrome or Safari.\n\nClick \"OK\" to continue. Click \"Cancel\" to leave.";
    var _mobileMessage = "This site is not optimized for mobile devices.\n\nClick \"OK\" to continue. Click \"Cancel\" to leave.";
    var _limits = {
        "IE": 9,
        "FF": 10,
        "Chrome": 4,
        "Safari": 5,
        "Opera": 11
    }
    return {
        alert: function (msg) {
            var result = window.confirm(msg);
            if (!result) {
                this.leave();
            }
        },
        test: function (/*Boolean*/ testForMobile) {
            if (testForMobile && (d.isIos || d.isAndroid)) {
                this.alert(_mobileMessage);
            } else {
                if (d.isFF < _limits.FF) {
                    this.alert(_unsupportedMessage);
                }
                if (d.isIE < _limits.IE) {
                    this.alert(_unsupportedMessage);
                }
                if (d.isChrome < _limits.Chrome) {
                    this.alert(_unsupportedMessage);
                }
                if (d.isSafari < _limits.Safari) {
                    this.alert(_unsupportedMessage);
                }
                if (d.isOpera < _limits.Opera) {
                    this.alert(_unsupportedMessage);
                }
            }
        },
        leave: function () {
            document.location = "http://trentu.ca/library/madgic/reha.htm";
        }
    }
})(dojo);