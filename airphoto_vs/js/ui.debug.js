﻿// file:            ui.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    app.ui = {
        // initialize the tabs in the HTML and apply behaviours
        _initTabs: function () {
            this.Tabs = new this.TabList("tabs");
            this.Tabs.setSelected("mainTab");
        },
        // create a menu panel and place the basemap switching contents in it
        _initBasemapSwitch: function () {
            var mp = new this.MenuPanel("search", $("#basemapSelector"));
            mp.place();
            mp.setContent($("<div>", {
                html: new this.BasemapContent().domNode
            }));
        },
        // init and create the content for the panel that holds links to other
        // university resources
        _initLinks: function(){
            var mp = new this.MenuPanel("links", $("#links"));
            var linkList = $("<div>")
                .addClass("lk-wrapper");
            $.each(app.config.LINKS, function (i, o) {
                var select = $("<div>")
                    .addClass("lk-selector")
                    //track
                    .click(function () {
                        app.ga.track({
                            "eventCategory": "Link",
                            "eventAction": "click",
                            "eventLabel": o["href"],
                            "eventValue": null
                        });
                    })
                    .appendTo(linkList);
                var link = $("<a>", {
                    "href": o["href"],
                    "target": "_blank"
                })
                    .append($("<img>", {
                        "src": o["img"]
                    }).addClass("lk-image"))
                    .append($("<span>", {
                        "html": o["label"]
                    }).addClass("lk-label"))
                    .appendTo(select);                   
            });
            mp.place();
            mp.setContent($("<div>", {
                html: linkList
            }));
        },
        //
        _initDialog: function() {
            $("#dialog").dialog();
            $("#dialog").dialog("option", "width", 375);
            $("#dialog").dialog("option", "height", 400);
            $(".ui-dialog button").prop("title", "");
            $("#dialog").dialog("close");
        },
        // 
        init: function() {
            this._initTabs();
            this._initBasemapSwitch();
            this._initLinks();
            this._initDialog();
            $(document).tooltip();
        },
        // empty all the search results
        clearDownloads: function () {
            $("#downloads").empty();
        },
        //
        openDialog: function() {
            $("#dialog").dialog("open");
        },
        //
        closeDialog: function() {
            $("#dialog").dialog("close");
        },
        //
        setDialogContent: function(content) {
            $("#dialog").html(content);
        },
        //
        setDialogFromAsyncUrl: function (url, title) {
            this.setDialogTitle(title);
            this.setDialogContent($("<img>", {
                src: "images/gear_5F89AA_FEFEFE_64x64.gif",
                style: "display: block; margin-left: auto; margin-right: auto;"
            }));
            var xhr = $.ajax({
                dataType: "json",
                url: url,
                timeout: 10000
            });
            $.notify.note("{0}: searching database for metadata".format(title), "info", 10);
            // for AGS Service
            //xhr.done($.proxy(function (data) {
            //    if (data.features.length > 0) {
            //        this.setDialogContent(app.ui.resultToNode(data));
            //        $.notify.note("{0}: found metadata!".format(title), "success", 10);
            //    } else {
            //        this.setDialogContent("no metadata found!");
            //        $.notify.note("{0}: no metadata found!".format(title), "error", 10);
            //    }
            //}, this));
            // for OpenShift
            // comment from here...
            xhr.done($.proxy(function (data) {
                if (data.results.length > 0) {
                    this.setDialogContent(app.ui.objectToNode(data.results));
                    $.notify.note("{0}: found metadata!".format(title), "success", 10);
                } else {
                    this.setDialogContent("no metadata found!");
                    $.notify.note("{0}: no metadata found!".format(title), "error", 10);
                }
            }, this));
            // ... to here to remove OpenShift stuff. also see views.debug.js.
            xhr.fail($.proxy(function (err) {
                this.setDialogContent("Error communicating with metadata database. (" + err.statusText + ")");
                $.notify.note("{0}: error querying the database".format(title), "error", 10);
            }, this));
        },
        //
        setDialogFromAsyncHtml: function (page, title) {
            this.setDialogTitle(title);
            this.setDialogContent($("<img>", {
                src: "images/gear_5F89AA_FEFEFE_64x64.gif",
                style: "display: block; margin-left: auto; margin-right: auto;"
            }));
            $("#dialog").load(page);
        },
        //
        setDialogTitle: function (content) {
            $("#dialog").dialog( "option", "title", content);
        },
        //
        onLayersLoadStart: function(){
            $("#layerLoader").css("display", "block");
        },
        //
        onLayersLoadEnd: function() {
            $("#layerLoader").css("display", "none");
        },
        //
        onLayersLoadSuccess: function (data) {
            this.onLayersLoadEnd();
            
            // add the year to each skeletal model
            var arr = $.each(data.menu[0].items, function (i, obj) {
                obj["year"] = obj["name"].substring(1);
            });
            // sort them so they're in chorological order
            arr.sort(function (a, b) {
                return (a.year > b.year) ? 1 : ((b.year > a.year) ? -1 : 0);
            }).reverse();
            // iterate and build the controllers from the model
            $.each(arr, function (i, service) {
                var controller = new app.controller.Controller(service);
                app.controller.add(controller);
                controller.initialize($("#layers"));
                controller.view.working(false);
            });
            $.notify.note("Found " + data.menu[0].items.length + " layers!", "success", 30);
        },
        //
        onLayersLoadError: function(err) {
            this.onLayersLoadEnd();
            $("#layers").html("Error: communication error with GIS server!");
            $.notify.note("Error: communication error with GIS server!", "error", 30);
            //madgic.util.log(err["statusText"]);
        },
        //
        animate: function() {
            var controllers = $.map($("input:checked", $(".view-photos-slider")), function (node, i) {
                return app.controller.find(node.id.split("_")[0]);
            });
            $.each(controllers, function (i, controller) {
                var i = 1000;
                var n = 1;
                var interval = setInterval(function () {                                       
                    controller.layer.setOpacity(n);
                    console.log(n);
                    n = n - (1 / 1000);
                    i = i - 1;
                    if (i == 0) {
                        console.log(0);
                        clearInterval(interval);
                    }
                }, 1);
            });
        },
        //
        nameToQuery: function (name) {
            var s = name.split("-");
            var p = {
                "ROLL": ((s[0].charAt(s[0].indexOf("A") + 1) == 0) ? s[0].replace("A0", "A") : s[0]),
                "PHOTO": s[1].replace(".tif", "").replace(/^0+/, "")
            };
            return "ROLL='" + p["ROLL"] + "' AND " + "PHOTO='" + p["PHOTO"] + "'";
        },
        //
        nameToJson: function(name) {
            var s = name.split("-");
            var p = {
                "roll": s[0].charAt(0) + parseInt(s[0].substring(s[0].indexOf(s[0].charAt(0))+1)),
                "photo": s[1].replace(".tif", "").replace(/^0+/, "")
            };
            return JSON.stringify(p);
        },
        //
        nameToId: function(name){
            var s = name.split("-");
            return s[0].replace(/^[A0]+/, "") + s[1].replace(/^0+/, "").replace(/\.tif$/, "");
        },
        //
        _sortAttributes: function (attr) {
            var keys = Object.keys(attr.attributes);
            var obj = attr.attributes;
            attr.attributes = {};
            var len = keys.length;
            keys.sort();
            for (var i = 0; i < len; i++) {
                k = keys[i];
                attr.attributes[k] = obj[k];
            }
            return attr;
        },
        //
        objectToNode: function (features) {            
            var wrapper = $("<div>");
            $.each(features, function (i, feature) {
                var feature = app.ui._sortAttributes(feature);
                var content = $("<div>");
                var i = 0;
                var len = $.map(feature.attributes, function (val, i) { return i; }).length
                var left = $("<div>", {
                    style: "float: left;"
                }).appendTo(content);
                var right = $("<div>", {
                    style: "float: right;"
                }).appendTo(content);

                for (var key in feature.attributes) {
                    var props = $("<div>", {
                        "html": "<b>" + key + "</b>: " + feature.attributes[key],
                        "class": (i % 2 != 0) ? "meta meta-highlight"  : "meta" 
                    });
                    if (i < (len / 2)) {
                        left.append(props);
                    } else {
                        right.append(props);
                    }
                    i++;
                }
                wrapper.append(content);
            });
            return wrapper;
        },
        //
        resultToNode: function (result) {
            var wrapper = $("<div>");
            $.each(result.features, function (i, feature) {
                var content = $("<div>");
                var i = 0;
                var len = $.map(feature.attributes, function (val, i) { return i; }).length
                var left = $("<div>", {
                    style: "float: left;"
                }).appendTo(content);
                var right = $("<div>", {
                    style: "float: right;"
                }).appendTo(content);
                for (var key in feature.attributes) {
                    var props = $("<div>", {
                        "html": "<b>" + key + "</b>: " + feature.attributes[key],
                        "class": (i % 2 != 0) ? "meta meta-highlight" : "meta"
                    });
                    if (i < (len / 2)) {
                        left.append(props);
                    } else {
                        right.append(props);
                    }
                    i++;
                }
                wrapper.append(content);
            });
            return wrapper;            
        },
        // to hold the list of tabs
        Tabs: null,
        // a tab list object that defines the UI functionality
        // of the tabs themselves *and* the content divs.
        // styles in main.css.
        TabList: function (/*String*/ nodeId) {
            // all the tabs
            var _tabList = $("#" + nodeId + " div");
            // iterate and define behaviours of the list
            // *and* associated div holding their content(s)
            $.each(_tabList, function (idx, node) {
                $(node).click(function () {
                    $("#" + nodeId + " div").removeClass("tab-selected");
                    $(this).addClass("tab-selected");
                    // omg i love jquery selectors. dojo is good too, just for the record. i mean that.
                    // TODO: user $.data here to get HTML5 elems.
                    $("div[data-tab-role]").css("display", "none");
                    $("div[data-tab-role='" + this.id + "']").css("display", "block");
                });
            });
            return {
                anchor: $("#" + nodeId),
                tabs: _tabList,
                panes: $("div[data-tab-role]"),
                setSelected: function (/*String*/ nodeId) {
                    this.anchor.children().removeClass("tab-selected");
                    $("#" + nodeId).addClass("tab-selected");
                    this.panes.css("display", "none");
                    $("div[data-tab-role='" + nodeId + "']").css("display", "block");
                },
                isSelected: function (/*DomNode*/ node) {
                    return $(el).hasClass("tab-selected");
                },
                destoryById: function (/*String*/ nodeId) {
                    $("#" + nodeId).remove();
                }
            }
        },
        // panel constructor for top bar drop-down items
        MenuPanel: function (/*String*/ placeholderName, /*domNode*/ toggle) {
            // figure the location and create an internal position obj
            var _x = $(toggle).offset().left;
            var _y = $(toggle).offset().top + $(toggle).height();
            var _pos = {
                x: _x + "px",
                y: _y + "px"
            };
            // the parent div to hold all the panel contents
            var anchor = $("<div></div>")
                .addClass("menu-anchor menu-panel")
                //.css("background-color", "#F2F5F7");
            // a loading spinner holding thing for panel that are to include AJAX content
            var loader = $("<div></div>", {
                "style": "display:none",
                "html": $("<img>", {
                    "src": "images/gear_5F89AA_FEFEFE_64x64.gif"
                })
            }).appendTo(anchor);
            // the actual content!
            var content = $("<div></div>", {
                "html": ""
            }).appendTo(anchor);
            // add a click handler to the suplied toggle node to hide/show the MenuPanel
            toggle.click(function () {
                if (anchor.css("display") == "none") {
                    $(".menu-anchor").css("display", "none");
                    anchor.css("display", "block");
                } else {
                    anchor.css("display", "none");
                }
            });
            // return as obj
            return {
                anchor: anchor,
                toggle: toggle,
                place: function () {
                    anchor.css({
                        "position": "absolute",
                        "top": _pos.y,
                        "left": _pos.x
                    }).appendTo($("body"));
                },
                show: function () {
                    anchor.css({ "display": "block" });
                },
                hide: function () {
                    anchor.css({ "display": "none" });
                },
                hideAll: function () {
                    $(".menu-anchor").css("display", "none");
                },
                setContent: function (node) {
                    content.html(node);
                },
                startLoading: function () {
                    loader.css("display", "block");
                },
                stopLoading: function () {
                    loader.css("display", "none");
                }
            }
        },
        // create the content for the basemap panel
        BasemapContent: function () {
            // this is the info we need for the basemaps
            var _config = [{
                title: "Topographic Map",
                subtitle: "",
                name: "topo",
                url: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer",
                img: "images/img_topo.png"
            }, {
                title: "Satellite & Aerial Imagery",
                subtitle: "",
                name: "satellite",
                url: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer",
                img: "images/img_imgy.png"
            }];
            var _cont = $("<div>").addClass("bm-wrapper");
            $.each(_config, function (idx, val) {
                var map = $("<div>")
                    .addClass("bm-selector")
                    .click(function (evt) {
                        $(".bm-selector").removeClass("bm-selected");
                        map.addClass("bm-selected");
                        app.map.Map.setBasemap(_config[idx].name);
                        app.ga.track({
                            "eventCategory": "Basemap",   
                            "eventAction": "switch",     
                            "eventLabel": _config[idx].name,
                            "eventValue": null
                        });
                    })
                    .appendTo(_cont);
                if (idx == 0) {
                    map.addClass("bm-selected");
                }
                // put the thumbnail in there
                map.append($("<img>", {
                    src: _config[idx].img
                }).addClass("bm-image"));
                // put the name in there
                // we can expand this to provide a subtitle (description) if we want
                map.append($("<span>", {
                    html: _config[idx].title
                }).addClass("bm-title"));
            });
            return {
                domNode: _cont,
                setSelected: function () {
                    // 
                },
                selected: function () {
                    //
                },
                isSelected: function () {
                    //
                }
            }
        }      
        
    } 
})(jQuery);