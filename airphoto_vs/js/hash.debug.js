﻿// file:            hash.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function () {
    app.hash = {
        _functionNames: ["go", "search"],
        callbacks: null,
        init: function (callbacks) {
            this.callbacks = callbacks;
        },       
        hasHash: function () {
            return (document.location.hash) ? true : false;
        },
        validate: function() {
            return true;
        },
        execute: function (hashObj) {
            for (var func in hashObj) {
                if ($.inArray(func, this._functionNames) >= 0) {
                    if (hashObj.hasOwnProperty("search")) {
                        this.callbacks[func](hashObj[func], hashObj["go"]);
                    } else {
                        this.callbacks[func](hashObj[func]);
                    }                   
                } else {
                    $.notify.note("{0} is not a valid function route".format(func), "error", 10);
                }                
            }
        },
        getHashAsObject: function () {
            var asArray = this.getHash().split("/");
            var asObject = new Object();
            $.each(this.getHash().split("/"), function (i, val) {
                if (i > 0) {
                    if (i % 2 != 0) {
                        asObject[val] = asArray[i + 1];
                    }
                }
            });
            return asObject;
        },
        getHash: function () {
            return document.location.hash;
        }
    }
})();