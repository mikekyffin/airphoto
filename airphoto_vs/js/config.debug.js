﻿// file:            config.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    // _base // move regex here to determine server
    app.config = {
        //
        APPLICATION_NAME: "airphoto",
        // where are we getting all the GIS servvice information from?
        MENU: "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/resources/services/getServiceDefinitions.ashx" + "?" + $.param({
            dir: "airphoto",
            f: "json"
        }),
        //
        SERVICE_BASE: "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/" + "airphoto",
        // resource path for menu info
        MENU_URL: "/handlers/menu.ashx",
        // resource path for menu info
        SEARCH_URL: "/handlers/search.ashx",
        // path to proxy
        PROXY: "handlers/proxy.ashx",
        // url for location search via NRCan
        GEO_SEARCH: "http://geogratis.gc.ca/loc/en/loc",
        // our geometry serverreha
        GEOM_SERVER: "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/Utilities/Geometry/GeometryServer",
        // clip & download vector 
        DOWNLOAD_VECTOR_URL: "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/tools/ClipFromWeb/GPServer/ClipFromWeb",
        // select & download raster
        DOWNLOAD_RASTER_URL: "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/arcgis/rest/services/tools/GetTile/GPServer/GetTile",
        // download authenitcation for zip file
        DOWNLOAD_AUTH: "http://web2.trentu.ca:2048/login?url=",
        // download license disclaimer, etc.
        DOWNLOAD_DISCLAIMER: "http://lonestar.trentu.ca/ORTHOS/login/ap_disclaimer.asp",
        // NAPL service (ours) - currently only exists on ALBERS
        METADATA: "http://albers.trentu.ca/arcgis/rest/services/NAPLIndex/NAPLIndex/MapServer/0/query",
        // OpenShift metadata db!
        METADATA_OS: "http://metadata-madgic.rhcloud.com/airphoto/photos/",
        // links - for the university links panel
        LINKS: [
            { 
                "img": "images/img_trent.png", 
                "label": "Trent University",
                "href": "http://www.trentu.ca/"
            },
            {
                "img": "images/img_bata.png",
                "label": "Bata Library",
                "href": "http://www.trentu.ca/library/"
            },
            {
                "img": "images/img_map.png",
                "label": "MaDGIC",
                "href": "http://www.trentu.ca/library/madgic/index.htm"
            },
            {
                "img": "images/img_reha.png",
                "label": "REHA",
                "href": "http://trentu.ca/library/madgic/reha.htm"
            }
        ]
    }
})(jQuery);