﻿// file:            Notify.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

(function ($) {
    // analagous to an enum of sub-types
    var _types = ["info", "warning", "error", "success"];
    // defaults and configuration settings
    var _config = {
        exceptions: {
            //
            NotifyNotATypeException: function (_in) {
                return {
                    name: "NotifyNotATypeException",
                    code: "N-001",
                    message: "Error: '" + _in + "' is not a supported note type."
                };
            },
            //
            NotifyInvalidPositionException: function (_in) {
                return {
                    name: "NotifyInvalidPositionException",
                    code: "N-002",
                    message: "Error: " + _in + " is not a supported note-stack position."
                };
            }
        },
        // default parameters
        defaults: {
            maxNotes: 4,
            timeout: 5000 // milliseconds
        },
        // helpful images
        icon: {
            success: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKfSURBVDjLpZPrS1NhHMf9O3bOdmwDCWREIYKEUHsVJBI7mg3FvCxL09290jZj2EyLMnJexkgpLbPUanNOberU5taUMnHZUULMvelCtWF0sW/n7MVMEiN64AsPD8/n83uucQDi/id/DBT4Dolypw/qsz0pTMbj/WHpiDgsdSUyUmeiPt2+V7SrIM+bSss8ySGdR4abQQv6lrui6VxsRonrGCS9VEjSQ9E7CtiqdOZ4UuTqnBHO1X7YXl6Daa4yGq7vWO1D40wVDtj4kWQbn94myPGkCDPdSesczE2sCZShwl8CzcwZ6NiUs6n2nYX99T1cnKqA2EKui6+TwphA5k4yqMayopU5mANV3lNQTBdCMVUA9VQh3GuDMHiVcLCS3J4jSLhCGmKCjBEx0xlshjXYhApfMZRP5CyYD+UkG08+xt+4wLVQZA1tzxthm2tEfD3JxARH7QkbD1ZuozaggdZbxK5kAIsf5qGaKMTY2lAU/rH5HW3PLsEwUYy+YCcERmIjJpDcpzb6l7th9KtQ69fi09ePUej9l7cx2DJbD7UrG3r3afQHOyCo+V3QQzE35pvQvnAZukk5zL5qRL59jsKbPzdheXoBZc4saFhBS6AO7V4zqCpiawuptwQG+UAa7Ct3UT0hh9p9EnXT5Vh6t4C22QaUDh6HwnECOmcO7K+6kW49DKqS2DrEZCtfuI+9GrNHg4fMHVSO5kE7nAPVkAxKBxcOzsajpS4Yh4ohUPPWKTUh3PaQEptIOr6BiJjcZXCwktaAGfrRIpwblqOV3YKdhfXOIvBLeREWpnd8ynsaSJoyESFphwTtfjN6X1jRO2+FxWtCWksqBApeiFIR9K6fiTpPiigDoadqCEag5YUFKl6Yrciw0VOlhOivv/Ff8wtn0KzlebrUYwAAAABJRU5ErkJggg==",
            info: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKcSURBVDjLpZPLa9RXHMU/d0ysZEwmMQqZiTaP0agoaKGJUiwIxU0hUjtUQaIuXHSVbRVc+R8ICj5WvrCldJquhVqalIbOohuZxjDVxDSP0RgzyST9zdzvvffrQkh8tBs9yy9fPhw45xhV5X1U8+Yhc3U0LcEdVxdOVq20OA0ooQjhpnfhzuDZTx6++m9edfDFlZGMtXKxI6HJnrZGGtauAWAhcgwVnnB/enkGo/25859l3wIcvpzP2EhuHNpWF9/dWs/UnKW4EOGDkqhbQyqxjsKzMgM/P1ymhlO5C4ezK4DeS/c7RdzQoa3x1PaWenJjJZwT9rQ1gSp/js1jYoZdyfX8M1/mp7uFaTR8mrt29FEMQILr62jQ1I5kA8OF59jIItVA78dJertTiBNs1ZKfLNG+MUHX1oaURtIHEAOw3p/Y197MWHEJEUGCxwfHj8MTZIcnsGKxzrIURYzPLnJgbxvG2hMrKdjItjbV11CYKeG8R7ygIdB3sBMFhkem0RAAQ3Fuka7UZtRHrasOqhYNilOwrkrwnhCU/ON5/q04vHV48ThxOCuoAbxnBQB+am65QnO8FqMxNCjBe14mpHhxBBGCWBLxD3iyWMaYMLUKsO7WYH6Stk1xCAGccmR/Ozs/bKJuXS39R/YgIjgROloSDA39Deit1SZWotsjD8pfp5ONqZ6uTfyWn+T7X0f59t5fqDhUA4ry0fYtjJcWeZQvTBu4/VqRuk9/l9Fy5cbnX+6Od26s58HjWWaflwkusKGxjm1bmhkvLXHvh1+WMbWncgPfZN+qcvex6xnUXkzvSiYP7EvTvH4toDxdqDD4+ygT+cKMMbH+3MCZ7H9uAaDnqytpVX8cDScJlRY0YIwpAjcNcuePgXP/P6Z30QuoP4J7WbYhuQAAAABJRU5ErkJggg==",
            warning: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIsSURBVDjLpVNLSJQBEP7+h6uu62vLVAJDW1KQTMrINQ1vPQzq1GOpa9EppGOHLh0kCEKL7JBEhVCHihAsESyJiE4FWShGRmauu7KYiv6Pma+DGoFrBQ7MzGFmPr5vmDFIYj1mr1WYfrHPovA9VVOqbC7e/1rS9ZlrAVDYHig5WB0oPtBI0TNrUiC5yhP9jeF4X8NPcWfopoY48XT39PjjXeF0vWkZqOjd7LJYrmGasHPCCJbHwhS9/F8M4s8baid764Xi0Ilfp5voorpJfn2wwx/r3l77TwZUvR+qajXVn8PnvocYfXYH6k2ioOaCpaIdf11ivDcayyiMVudsOYqFb60gARJYHG9DbqQFmSVNjaO3K2NpAeK90ZCqtgcrjkP9aUCXp0moetDFEeRXnYCKXhm+uTW0CkBFu4JlxzZkFlbASz4CQGQVBFeEwZm8geyiMuRVntzsL3oXV+YMkvjRsydC1U+lhwZsWXgHb+oWVAEzIwvzyVlk5igsi7DymmHlHsFQR50rjl+981Jy1Fw6Gu0ObTtnU+cgs28AKgDiy+Awpj5OACBAhZ/qh2HOo6i+NeA73jUAML4/qWux8mt6NjW1w599CS9xb0mSEqQBEDAtwqALUmBaG5FV3oYPnTHMjAwetlWksyByaukxQg2wQ9FlccaK/OXA3/uAEUDp3rNIDQ1ctSk6kHh1/jRFoaL4M4snEMeD73gQx4M4PsT1IZ5AfYH68tZY7zv/ApRMY9mnuVMvAAAAAElFTkSuQmCC",
            error: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJPSURBVDjLpZPLS5RhFMYfv9QJlelTQZwRb2OKlKuINuHGLlBEBEOLxAu46oL0F0QQFdWizUCrWnjBaDHgThCMoiKkhUONTqmjmDp2GZ0UnWbmfc/ztrC+GbM2dXbv4ZzfeQ7vefKMMfifyP89IbevNNCYdkN2kawkCZKfSPZTOGTf6Y/m1uflKlC3LvsNTWArr9BT2LAf+W73dn5jHclIBFZyfYWU3or7T4K7AJmbl/yG7EtX1BQXNTVCYgtgbAEAYHlqYHlrsTEVQWr63RZFuqsfDAcdQPrGRR/JF5nKGm9xUxMyr0YBAEXXHgIANq/3ADQobD2J9fAkNiMTMSFb9z8ambMAQER3JC1XttkYGGZXoyZEGyTHRuBuPgBTUu7VSnUAgAUAWutOV2MjZGkehgYUA6O5A0AlkAyRnotiX3MLlFKduYCqAtuGXpyH0XQmOj+TIURt51OzURTYZdBKV2UBSsOIcRp/TVTT4ewK6idECAihtUKOArWcjq/B8tQ6UkUR31+OYXP4sTOdisivrkMyHodWejlXwcC38Fvs8dY5xaIId89VlJy7ACpCNCFCuOp8+BJ6A631gANQSg1mVmOxxGQYRW2nHMha4B5WA3chsv22T5/B13AIicWZmNZ6cMchTXUe81Okzz54pLi0uQWp+TmkZqMwxsBV74Or3od4OISPr0e3SHa3PX0f3HXKofNH/UIG9pZ5PeUth+CyS2EMkEqs4fPEOBJLsyske48/+xD8oxcAYPzs4QaS7RR2kbLTTOTQieczfzfTv8QPldGvTGoF6/8AAAAASUVORK5CYII=",
            close: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAAAlwSFlzAAAOwwAADsMBx2+oZAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuODc7gF0AAAHASURBVDhPlVPXisJQEJWFhcv+xD4v+YcFv8KCiIgiChp7QWwP9o69YQUX/MXI2ZkLuhuSPBi4ZCBzypy5sdlefPr9/oclZLfbfdJRrBoIrLTbbbVer38behh8OByw3++xWq0MJL1eTxkMBphMJqjVaj/lcvnrSfIAXy4X8NlsNphOp0+SbrerkDoWiwXO5zO4LhQKaiaTeZMkRCAIdD8ej7jdbmAnRABSVDqdjkLqmM/nOJ1O4JrAGoHfdWOQbUFNGpHher1iu91iOByC1DGbzcDkXOfzeS2dTgvTnGg+MRqNNLbKo7AiAzkXcoJcLmcNfjCSRdFqteQ4bJeB7Cqbzd5TqZS58n87BOZVSVWq0Ww2sV6veW7EYjHLFUsOalYYNB6PZQa8DcoGy+UStHskk0mEQiFzkkajIcGUgQSyMoWlxePxe6VSkVupVquIRCLw+Xx6EmJXGMCps10ie4BFNBoV4XBYo/Sls2KxiGAwCLfb/UdCN8vOFvmm8ZvC0lRVfQYWCASE3+/nDaBUKvEYcDqddt0qKSQ7f6Q57xSWIW2v1ys8Ho9mCn4wJRIJO1u2+plcLpdwOBx65Rf/ZkP7L6S1P2NS3cqVAAAAAElFTkSuQmCC"
        }
    };
    $.notify = {
        // applies style for the stack in the DOM.
        // see styles in notify.css
        init: function (node) {
            $(node).addClass("notify-stack");
            this._node = node;
        },
        //
        note: function (text, type, timeout) {
            try {
                if ($.inArray(type, _types) == -1) {
                    throw new _config.exceptions.NotifyNotATypeException(type);
                }
                if (this._count() == _config.defaults.maxNotes) {
                    this._clearLast();
                }
                // the root of the note
                var parent = $("<div>")
                    .addClass("notify-note notify-" + type)
                    .appendTo(this._node)
                    .hover(function () {
                        $(".notify-close img", this).css("opacity", "1");
                    }, function () {
                        $(".notify-close img", this).css("opacity", "0");
                    })
                    .fadeOut(timeout ? timeout*1000  : _config.defaults.timeout, function () {
                        $(this).remove();
                    });                
                // the status type icon
                $("<span>", {
                    html: $("<img>", {
                        src: _config.icon[type] 
                    })
                })
                    .addClass("notify-icon")
                    .appendTo(parent);
                // the text message itself - could also be a node
                $("<span>", {
                    html: text
                })
                    .addClass("notify-message")
                    .appendTo(parent);
                // the close button that shows up on hover
                $("<span>", {
                    html: $("<img>", {
                        src: _config.icon["close"],
                        click: function () {
                            // sooooooo stupid. el stupido.
                            $(this).parent().parent().remove();
                        }
                    })
                    
                })
                    .addClass("notify-close")
                    .appendTo(parent);
                // return for further chaining?
                return parent;
            } 
            catch (e)
            {
                console.log(e);
                console.log(e.name + ": " + e.message);
                return null;
            }          
        },
        //
        repositionAll: function (position) {
            try {
                if (position === "top")
                {
                    this._node.removeAttr("bottom");
                    this._node.css("top", "3px");
                } else if (position === "bottom") {
                    this._node.removeAttr("top");
                    this._node.css("bottom", "3px");
                } else {
                    throw new _config.exceptions.NotifyInvalidPositionException(position);
                }
            } catch (e) {
                console.log(e);
                console.log(e.name + ": " + e.message);
            }
        },
        // remove all notes
        clearAll: function() {
            this._node.empty();
        }, 
        // remove only the last note - we're using this so there are never any more
        // than the maximum number of allowed notes in the _config above. this could 
        // changed as desired.
        _clearLast: function() {
            $(".notify-note", this._node).last().remove();
        },
        _count: function() {
            return $(".notify-note", this._node).length;
        },
        _node: null
    }
}(jQuery));