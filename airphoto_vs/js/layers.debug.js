﻿// file:            layers.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {

    function Layer(options) {
        esri.layers.ArcGISDynamicMapServiceLayer.call(this, options.url, {
            "id": options.name + "_layer",
            "visible": false
        });
        this.year = parseInt(options.year);
        this.setVisibleLayers([0]);
    }
    Layer.prototype = Object.create(esri.layers.ArcGISDynamicMapServiceLayer.prototype);
    Layer.prototype._arrayToRange = function (arr) {
        var str = "(";
        for (var i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                str += ("'" + arr[i] + "'");
            }
            else {
                str += ("'" + arr[i] + "',");
            }
        }
        str += ")";
        return str;
    };
    // take the array given and string-format it for use as an ArcGIS-style layer definition
    Layer.prototype.setTiles = function (array) {
        // just set the object's internal list of tiles. it's good to know.
        this.displayedTiles = array;
        // the string magic
        var range = this._arrayToRange(array);
        // note that we're implicitly pushing to index [0] because that's the only layer within the servce. if we had other layers
        // and/or the tiles weren't in [0], we couldn't do this - we'd have to actually supply the index.
        var layerDefinitions = [];
        layerDefinitions.push("Name IN " + range);
        this.setLayerDefinitions(layerDefinitions, false);
        app.ga.track({
            "eventCategory": "Layer",
            "eventAction": "render",
            "eventLabel": array.length,
            "eventValue": null
        });
    };

    app.layers = {
        // see above
        Layer: Layer,
		// return the models from the menu service - this
		// finds out which services we have available. because
		// ww're not adding a collection to the footprint 
		// services, these are not found. that's just fine!
		_load: function () {
			return $.getJSON(app.config.PROXY + "?" + app.config.MENU);
		},
		// set the layers list a jQuery sortable object list - drag and drop!
		_makeSortable: function () {
			$("#layers").sortable().on("sortstop", function (evt, ui) {
				var k, lyr;
				var lyrList = $.map($("#layers div.view"), function (val, idx) {
					return $(val).attr("layer");
				});
				for (k = lyrList.length - 1; k > -1; k--) {
				    var layer = app.map.Map.getLayer(lyrList[k]);
				    var footprint = app.map.Map.getLayer(lyrList[k].replace("_layer", "_footprint"));
				    app.map.Map.reorderLayer(layer, app.map.Map.layerIds.length - 1);
					app.map.Map.reorderLayer(footprint, app.map.Map.layerIds.length - 2);
				}
			});
		},
		// start the layer task - on success sends response to build controllers
		init: function () {
		    app.ui.onLayersLoadStart();
			var services = this._load();
			this._makeSortable();
			services.done($.proxy(function (data) {
			    this.onLayersLoadSuccess(data);
                // publish that layers are loaded!
				dojo.publish("layers", [{ loaded: true }]);
			}, app.ui));
			services.fail($.proxy(function (error) {
			    this.onLayersLoadError(error);
			}, app.ui));
		}
	}
}(jQuery));