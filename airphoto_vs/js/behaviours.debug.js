﻿// file:            behaviours.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    app.behaviours = {
        // initialize any behaviours not already internal to specific objects/elements
        init: function () {
            // open and close the right content pane
            $("#toggle").bind("click", function () {
                if ($("#right").hasClass("menuOpen")) {
                    // close the menu
                    $("#right").addClass("menuClosed");
                    $("#right").removeClass("menuOpen");
                    $("#arrow").addClass("arrow-left");
                    $("#arrow").removeClass("arrow-right");
                    dijit.byId("map").resize();
                    dijit.byId("bc").resize();
                    // track this behaviour
                    app.ga.track({
                        "eventCategory": "Toggle",
                        "eventAction": "click",
                        "eventLabel": "menu close",
                        "eventValue": null
                    });

                } else {
                    // open the menu
                    $("#right").addClass("menuOpen");
                    $("#right").removeClass("menuClosed");
                    $("#arrow").addClass("arrow-right");
                    $("#arrow").removeClass("arrow-left");
                    dijit.byId("map").resize();
                    dijit.byId("bc").resize();
                    // track this behaviour
                    app.ga.track({
                        "eventCategory": "Toggle",
                        "eventAction": "click",
                        "eventLabel": "menu open",
                        "eventValue": null
                    });
                }
            });
            // clear the list of downloads in the download tab
            $("#downloadClear").click(function () {
                app.ui.clearDownloads();
            });
            // open the Maps & Data portion of the REHA web presence
            $("#toDataMaps").click(function () {
                var link = "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/reha"
                app.ga.track({
                    "eventCategory": "Link",
                    "eventAction": "click",
                    "eventLabel": link,
                    "eventValue": null
                });
                window.open(link);
            });
            // open the dialog with the about page content
            $("#about").click(function () {
                app.ui.setDialogFromAsyncHtml("about.html", "About REHA");
                app.ui.openDialog();
                app.ga.track({
                    "eventCategory": "Link",
                    "eventAction": "click",
                    "eventLabel": "about.html",
                    "eventValue": null
                });
            });
            // open the dialog with the help page content
            $("#help").click(function () {
                app.ui.setDialogFromAsyncHtml("help.html", "Help");
                app.ui.openDialog();
                app.ga.track({
                    "eventCategory": "Link",   
                    "eventAction": "click",      
                    "eventLabel": "help.html",
                    "eventValue": null
                });
            });
            //create a notifaction area at the node provided
            $.notify.init($("#notify"));
        }
    }
})(jQuery);