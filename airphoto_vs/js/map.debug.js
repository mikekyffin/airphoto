﻿// file:            map.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    //
    app.map = {
        // reference to the map
        Map: null,
        // create the map
        init: function () {
            var popup = new esri.dijit.Popup(null, dojo.create("div"));
            this.Map = new esri.Map("map", {
                infoWindow: popup,
                basemap: "topo",
                center: [-78.324137, 44.304319], //long, lat
                zoom: 9,
                sliderStyle: "small",
                logo: true,
                showAttribution: false
            });

            dojo.connect(this.Map, "onUpdateEnd", this, function () {
                $("#spinner").css("display", "none");
                app.views.setTilesUpdateEnd();
            });

            dojo.connect(this.Map, "onUpdateStart", this, function () {
                $("#spinner").css("display", "block");
                app.views.setTilesUpdateStart();
            });

            // the main click handler
            dojo.connect(this.Map, "onLoad", this, function () {
                dojo.connect(this.Map, "onClick", app.find, function (evt) {
                    app.map.LastClick = evt;
                    $.each(app.controller.Controllers, function (i, controller) {
                        if (controller.view.isChecked()) {
                            var hook = dojo.connect(controller.layer, "onUpdateEnd", this, function () {
                                controller.identify(evt);
                                dojo.disconnect(hook);
                            });
                            controller.layer.setTiles([]);                           
                        }
                    });
                });                
            });

            dojo.connect(this.Map, "onResize", this, function (evt) {
                // $("#notify").notify().reposition();
            });

            // add this to force Dojo to add a random variable to GP GET URLs. important for IE11.
            // http://forums.arcgis.com/threads/88625-IE-caching-responses-to-GP.submitJob-callbacks?highlight=submitjob
            esri.setRequestPreCallback(function (ioArgs) {
                if (ioArgs.url.indexOf("GPServer") > -1) {
                    ioArgs.preventCache = true;
                }
                return ioArgs;
            });
        },
        // add a layer to the map
        addLayer: function (/*esri.Layer*/ layer) {
            this.Map.addLayer(layer);
        },
        // remove a layer from the map
        removeLayer: function (/*esri.Layer*/ layer) {
            this.Map.removeLayer(layer);
        },
        // check to see if the layer is already on the map using the layers ID. note: needs ID.
        hasLayer: function (layerId) {
            return $.inArray(layerId, this.Map.layerIds) >= 0;
        },
        // zoom to the extent of a layer
        zoomLayer: function (/*esri.Layer*/ layer) {
            this.Map.setExtent(layer.fullExtent);
        },
        LastClick: null
    }
})(jQuery);