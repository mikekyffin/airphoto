﻿// file:            init.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!
 
var app = app || {};
(function () {
    //
    app.init = function () {
        
        // reference the proxy and enable it
        esri.config.defaults.io.proxyUrl = app.config.PROXY;
        esri.config.defaults.io.alwaysUseProxy = true;
        //
        app.map.init();
        app.layers.init();
        app.ui.init();
        app.behaviours.init();
        // subscribe to the layers channel so that we can know if the controllers
        // (and their vew/DOM representations) are all loaded
        dojo.subscribe("layers", function (data) {
            // if there's a hash in the url *and* everything has been loaded
            if (app.hash.hasHash && data.loaded == true) {
                // send an object with hash function definitions to the app.hash object.
                // if you need to edit these functions, do so here.
                app.hash.init({
                    // forward the map to a location given
                    go: function () {
                        try {
                            // split the values into an array of floats
                            var args = arguments[0].split(",").map(function (val, i) {
                                return parseFloat(val);
                            });
                            if (args.length < 2) {
                                $.notify.note("not enough arguments in function route", "error", 10);
                            }
                            else if (args.length > 3) {
                                $.notify.note("too many arguments in function route", "error", 10);
                            }
                            else {
                                (args.length == 2) ? app.map.Map.centerAt(new esri.geometry.Point(args[1], args[0])) : app.map.Map.centerAndZoom(new esri.geometry.Point(args[1], args[0]), args[2]);                                                                      
                                app.ga.track({
                                    "eventCategory": "Route",
                                    "eventAction": "go",
                                    "eventLabel": args,
                                    "eventValue": null
                                });
                            }                            
                        } catch (e) {
                            $.notify.note("Error:" + e.message, "error", 10);
                        }
                    },
                    // search for data from available services at the location given.
                    // this *needs* to have the accompanyin go has function available.
                    // it wont work without it.
                    // to-do: handle the error where a location isn't supplied.
                    search: function () { 
                        var arr = $.map(arguments[0].split(","), function (val, i) {
                            return $.trim(val);
                        });
                        var ll = arguments[1];
                        $.each(arr, function (i, year) {
                            if (year.length === 4) {
                                var latLng = ll.split(",").map(function (v, i) {
                                    return parseFloat(v);
                                });
                                var webMer = esri.geometry.lngLatToXY(latLng[1], latLng[0]);
                                var controller = app.controller.find("y" + year);
                                controller.view.setChecked(true);
                                controller.identify({
                                    mapPoint: {
                                        x: webMer[0],
                                        y: webMer[1]
                                    }
                                });
                            } else {
                                $.notify.note("Error: year " +  year + " not on record", "error", 10);
                            }
                            app.ga.track({
                                "eventCategory": "Route",
                                "eventAction": "search",
                                "eventLabel": year,
                                "eventValue": null
                            });
                        });
                    }                           
                });
                var urlHash = app.hash.getHashAsObject();
                app.hash.execute(urlHash);
            }
        });
        // this needs to go somewhere else
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
                if (m == "{{") { return "{"; }
                if (m == "}}") { return "}"; }
                return args[n];
            });
        };
    }
}());