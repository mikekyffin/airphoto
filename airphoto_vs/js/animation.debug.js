﻿// file:            animation.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    //
    app.animation = {
        _sortLayerArray: function (a, b) {
            return ((a.year < b.year) ? -1 : ((a.year > b.year) ? 1 : 0))
        },
        //
        AnimationTargets: function (/* esri.Map */ Map) {
            return $.map($(".view-photos-slider input:checked"), function (val, i) {
                return Map.getLayer(val.name);
            });
        },
        //
        AnimationParameters: function (/*object*/ params) {
            return {
                start: (params["start"]) ? params["start"] / 100 : 100,
                end: (params["end"]) ? params["end"] / 100 : 100,
                speed: (params["milliseconds"]) ? params["milliseconds"] : 100
            }
        },
        //
        animate: function (params, targets) {
            var sorted = targets.sort(this._sortLayerArray);
            $.each(sorted, function (i, layer) {
                var o = 1;
                layer.setOpacity(o);
                var sid = setInterval(function () {
                    if (o == 0) {
                        clearInteval(sid)
                    } else {
                        layer.setOpacity(o);
                        o -= 0.01;
                    }
                }, 100);
            });

        }
    }

})(jQuery);