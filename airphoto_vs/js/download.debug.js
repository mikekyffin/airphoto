﻿// file:            download.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    app.download = {
        // a boolean for the ui - has the user visited the download tab yet?
        hasVisited: false,
        // create a set of parameters for the for the download operation
        ImageDownloadParameters: function (_name, _year) {
            var result = {
                title: null,
                year: null
            };
            try {
                if (!_name || !_year) {
                    throw new app.exceptions.DownloadParameterException();
                } else {
                    result["title"] = _name.substring(0, _name.lastIndexOf("."));
                    result["year"] = "y"+_year;
                }
            } catch (e) {
                madgic.util.log(e.name + ", " + e.message); 
            } finally {
                // return the result no matter what - could be empty (have null values)
                return result;
            }
        },
        // 
        ImageDownload: function (params) {
            // this is the visible part in the download list
            var _view = new app.views.DownloadView(params);
            _view.place($("#downloads"));
            _view.statusIcon("info");
            // an esri geoprocessing task
            var _gp = new esri.tasks.Geoprocessor(app.config.DOWNLOAD_RASTER_URL);

            var _jobStatus = function (jobInfo) {
                if (jobInfo.messages.length > 0) {
                    if (jobInfo.jobStatus !== "esriJobFailed") {
                        _view.addMessage(jobInfo.messages[jobInfo.messages.length - 1].description);
                    } else {
                        _jobError(jobInfo);
                    }
                }
            };
            // this usually only fires if there's a problem accessing the service itself,
            // so we checked for esriJobFailed in other places and call this if needed.
            var _jobError = function (jobInfo) {
                _view.onError(jobInfo.messages[jobInfo.messages.length - 1].description);
            };

            var _jobComplete = function (jobInfo) {
                if (jobInfo.jobStatus !== "esriJobFailed") {
                    _gp.getResultData(jobInfo.jobId, "Output", function (data) {
                        _view.onComplete(data);
                    }, function (err) {
                        _jobError(jobInfo);
                    });
                }
            };
            return {
                // send the task to the server
                submitJob: function () {
                    _gp.submitJob({
                        Tile: params["title"],
                        Project: app.config.APPLICATION_NAME + "/" + params["year"],
                        f: "json"
                    }, _jobComplete, _jobStatus, _jobError);
                }
            }
        }
    };
})(jQuery);