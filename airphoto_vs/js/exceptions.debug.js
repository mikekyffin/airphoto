﻿// file:            exceptions.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function () {
    app.exceptions = {
        DownloadParameterException: function() {
            return {
                name: "DownloadParameterException",
                code: "D-001",
                message: "Error: missing or incorrect download parameter."
            }
        }
    }
})();