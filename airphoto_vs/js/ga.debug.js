﻿// file:            ga.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    app.ga = {
        // from 'https://developers.google.com/analytics/devguides/collection/analyticsjs/events'
        //ga('send', {
        //    'hitType': 'event',          // Required.
        //    'eventCategory': 'button',   // Required.
        //    'eventAction': 'click',      // Required.
        //    'eventLabel': 'nav buttons',
        //    'eventValue': 4
        //});
        track: function (obj) {        
            ga("send", $.extend({
                "hitType": "event"               
            }, obj));
        }           
    }
})(window.jQuery);